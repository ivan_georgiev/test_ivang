package com.ebay.marketplace.search.v1.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.7
 * 2013-10-28T14:18:34.637+02:00
 * Generated source version: 2.7.7
 * 
 */
@WebService(targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", name = "FindingServicePortType")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface FindingServicePortType {

    @WebResult(name = "findItemsByProductResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsByProduct")
    public FindItemsByProductResponse findItemsByProduct(
        @WebParam(partName = "messageParameters", name = "findItemsByProductRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsByProductRequest messageParameters
    );

    @WebResult(name = "getSearchKeywordsRecommendationResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/getSearchKeywordsRecommendation")
    public GetSearchKeywordsRecommendationResponse getSearchKeywordsRecommendation(
        @WebParam(partName = "messageParameters", name = "getSearchKeywordsRecommendationRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        GetSearchKeywordsRecommendationRequest messageParameters
    );

    @WebResult(name = "getHistogramsResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/getHistograms")
    public GetHistogramsResponse getHistograms(
        @WebParam(partName = "messageParameters", name = "getHistogramsRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        GetHistogramsRequest messageParameters
    );

    @WebResult(name = "findItemsIneBayStoresResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsIneBayStores")
    public FindItemsIneBayStoresResponse findItemsIneBayStores(
        @WebParam(partName = "messageParameters", name = "findItemsIneBayStoresRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsIneBayStoresRequest messageParameters
    );

    @WebResult(name = "findItemsByImageResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsByImage")
    public FindItemsByImageResponse findItemsByImage(
        @WebParam(partName = "messageParameters", name = "findItemsByImageRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsByImageRequest messageParameters
    );

    @WebResult(name = "findItemsByCategoryResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsByCategory")
    public FindItemsByCategoryResponse findItemsByCategory(
        @WebParam(partName = "messageParameters", name = "findItemsByCategoryRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsByCategoryRequest messageParameters
    );

    @WebResult(name = "findCompletedItemsResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsAdvanced")
    public FindCompletedItemsResponse findCompletedItems(
        @WebParam(partName = "messageParameters", name = "findCompletedItemsRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindCompletedItemsRequest messageParameters
    );

    @WebResult(name = "findItemsForFavoriteSearchResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsForFavoriteSearch")
    public FindItemsForFavoriteSearchResponse findItemsForFavoriteSearch(
        @WebParam(partName = "messageParameters", name = "findItemsForFavoriteSearchRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsForFavoriteSearchRequest messageParameters
    );

    @WebResult(name = "getVersionResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/getVersion")
    public GetVersionResponse getVersion(
        @WebParam(partName = "messageParameters", name = "getVersionRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        GetVersionRequest messageParameters
    );

    @WebResult(name = "findItemsAdvancedResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsAdvanced")
    public FindItemsAdvancedResponse findItemsAdvanced(
        @WebParam(partName = "messageParameters", name = "findItemsAdvancedRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsAdvancedRequest messageParameters
    );

    @WebResult(name = "findItemsByKeywordsResponse", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services", partName = "messageParameters")
    @WebMethod(action = "http://www.ebay.com/marketplace/search/v1/services/findItemsByKeywords")
    public FindItemsByKeywordsResponse findItemsByKeywords(
        @WebParam(partName = "messageParameters", name = "findItemsByKeywordsRequest", targetNamespace = "http://www.ebay.com/marketplace/search/v1/services")
        FindItemsByKeywordsRequest messageParameters
    );
}
