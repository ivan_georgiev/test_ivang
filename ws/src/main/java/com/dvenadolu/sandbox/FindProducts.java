package com.dvenadolu.sandbox;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import com.ebay.marketplace.search.v1.services.FindItemsByCategoryRequest;
import com.ebay.marketplace.search.v1.services.FindItemsByKeywordsRequest;
import com.ebay.marketplace.search.v1.services.FindItemsByKeywordsResponse;
import com.ebay.marketplace.search.v1.services.FindingService;
import com.ebay.marketplace.search.v1.services.FindingServicePortType;

/*
 * 	Авто-Мото-Вело 6001
	Бижутерия, Часовници 281
	Детски стоки 2984
	Дом, Градина 11700
	Електроника, Техника 293
	Изкуство, Музика 550
	Книжарница 267
	Козметика, Здраве 26395
	Колекционерство 1
	Компютри 58058
	Мода, Облекло 11450
	Спорт, Развлечения 382
	Телефони, GSM 15032
 */


public class FindProducts {
	 private static final QName SERVICE_NAME = new QName("http://www.ebay.com/marketplace/search/v1/services", "FindingService");

	    private FindProducts () {
	    }

	    public static void main(String args[]) throws java.lang.Exception {
	        URL wsdlURL = FindingService.WSDL_LOCATION;
	        if (args.length > 0 && args[0] != null && !"".equals(args[0])) { 
	            File wsdlFile = new File(args[0]);
	            try {
	                if (wsdlFile.exists()) {
	                    wsdlURL = wsdlFile.toURI().toURL();
	                } else {
	                    wsdlURL = new URL(args[0]);
	                }
	            } catch (MalformedURLException e) {
	                e.printStackTrace();
	            }
	        }
	      
	        FindingService ss = new FindingService(wsdlURL, SERVICE_NAME);
	        FindingServicePortType port = ss.getFindingServiceSOAPPort();
	        BindingProvider bp = (BindingProvider) port;
	        Map<String, Object> requestProperties = bp.getRequestContext();
	        Map<String, List<String>> httpHeaders = new HashMap<String, List<String>>();
	        
	     	//Set the HTTP headers
	        httpHeaders.put("X-EBAY-SOA-MESSAGE-PROTOCOL", Collections.singletonList("SOAP12"));
	        httpHeaders.put("X-EBAY-SOA-OPERATION-NAME", Collections.singletonList("findItemsByKeywords"));
	        httpHeaders.put("X-EBAY-SOA-SECURITY-APPNAME", Collections.singletonList("ABC68cf18-e323-491b-afdf-380fd09ffaa"));
	        
	        requestProperties.put(MessageContext.HTTP_REQUEST_HEADERS, httpHeaders);
	        requestProperties.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://svcs.sandbox.ebay.com/services/search/FindingService/v1");
	        
	        FindItemsByKeywordsRequest fikr = new FindItemsByKeywordsRequest();	        
	        fikr.setKeywords("apple");
	        
	        FindItemsByCategoryRequest ficr = new FindItemsByCategoryRequest();
	        ficr.getCategoryId().add("1");
	        
	        FindItemsByKeywordsResponse resp = port.findItemsByKeywords(fikr);

	        
	        System.exit(0);
	    }

}
